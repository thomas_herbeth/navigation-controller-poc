package com.booboot.ncp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.my_fragment.*

class MyFragment : Fragment() {
    private var myId: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.my_fragment, container, false)
        /* myId is always 1 here */
        myId = MyFragmentArgs.fromBundle(arguments).myId
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /* Always displays "My Fragment 1" */
        textView.text = "My Fragment $myId"
    }
}